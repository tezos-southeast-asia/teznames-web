import React from 'react';
import { shallow } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import Home from '../pages/home/home';

describe('<Home />', () => {
  const mockStore = configureMockStore([]);
  const store = mockStore({
    Root: {
      hideLoading: true
    },
    Search: {
      searchVal: undefined,
      owner: undefined,
      destination: undefined
    }
  });

  const wrapper = shallow(
    <MemoryRouter>
      <Provider store={store}>
        <Home />
      </Provider>
    </MemoryRouter>
  );

  test('Renders without crashing', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
