import React from 'react';
import { shallow } from 'enzyme';
import Theme from '../assets/themes/theme';
import MainLayout from '../layouts/mainLayout';

describe('<MainLayout />', () => {
  const wrapper = shallow(
    <MainLayout>
      <h1>Jest Test</h1>
    </MainLayout>
  );
  it('Correct theme is used', () => {
    expect(wrapper.find('ThemeProvider').prop('theme')).toBe(Theme);
  });
  it('Correct theme is used', () => {
    expect(wrapper.find('ThemeProvider').prop('theme')).toBe(Theme);
  });
  test('Renders without crashing', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
