import React from 'react';
import { shallow } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import Registration from '../pages/registration/registration';
import RegisterableCard from '../pages/registration/registerableCard';

describe('<RegisterableCard />', () => {
  const wrapper = shallow(<RegisterableCard registerable searchValue="abcd" />);

  test('Props are correct', () => {
    expect(wrapper.childAt(0).props().title).toBe('abcd.tez is available.');
  });

  test('Renders without crashing', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

describe('<Registration />', () => {
  const mockStore = configureMockStore([]);
  const store = mockStore({
    Root: {
      hideLoading: true
    },
    Search: {
      searchVal: undefined,
      owner: undefined,
      destination: undefined
    }
  });
  const wrapper = shallow(
    <MemoryRouter>
      <Provider store={store}>
        <Registration />
      </Provider>
    </MemoryRouter>
  );

  test('Renders without crashing', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
