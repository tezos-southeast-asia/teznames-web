import React from 'react';
import { shallow } from 'enzyme';
import Routes from '../routes';

describe('<Routes />', () => {
  const wrapper = shallow(<Routes />);

  it('Switch and routes', () => {
    expect(wrapper.find('Switch').exists()).toBe(true);
    expect(wrapper.find('Route')).toHaveLength(8);
  });

  test('Renders without crashing', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
