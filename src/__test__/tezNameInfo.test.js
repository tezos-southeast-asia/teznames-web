import React from 'react';
import { shallow } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import ListItem from '@material-ui/core/ListItem';
import TezNameInfo from '../pages/tezNameInfo/tezNameInfo';
import CodeHighLighter from '../components/codeHighLighter';
import OwnerInfoListItem from '../pages/tezNameInfo/ownerInfoListItem';

describe('<CodeHighLighter />', () => {
  const wrapper = shallow(<CodeHighLighter hidden code="testing" />);

  test('Renders without crashing', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

describe('<OwnerInfoListItem />', () => {
  const wrapper = shallow(
    <OwnerInfoListItem
      className="className"
      icon="owner"
      primaryTxt="Owner"
      secondaryEle="txewg1"
    />
  );

  it('Props are correct', () => {
    const listItemProps = wrapper
      .find(ListItem)
      .childAt(1)
      .props();
    expect(listItemProps.primary).toBe('Owner');
    expect(listItemProps.secondary).toBe('txewg1');
  });

  test('Renders without crashing', () => {
    expect(wrapper).toMatchSnapshot();
  });
});

describe('<TezNameInfo />', () => {
  const mockStore = configureMockStore([]);
  const store = mockStore({
    Root: {
      hideLoading: true
    },
    Search: {
      searchVal: undefined,
      owner: undefined,
      destination: undefined
    }
  });
  const wrapper = shallow(
    <MemoryRouter>
      <Provider store={store}>
        <TezNameInfo />
      </Provider>
    </MemoryRouter>
  );

  test('Renders without crashing', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
