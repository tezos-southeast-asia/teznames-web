import connection from '../configs/connection';
import FetchApiData from '../helpers/api';

export async function getWaitListObj(searchVal) {
  const waitListObj = {
    onWaitList: false,
    owner: '',
    dest: ''
  };
  const registrationList = await FetchApiData(connection.registrationList);
  registrationList.forEach(el => {
    if (el.name.split('.')[0] === searchVal) {
      waitListObj.onWaitList = true;
      waitListObj.owner = el.address;
      waitListObj.dest = el.address;
    }
  });
  return waitListObj;
}

export async function checkIsRegistered(searchVal) {
  const registered = await FetchApiData(`${connection.isRegistered + searchVal}.tez`);
  return registered.isRegistered;
}

export async function checkRegisterable(searchVal) {
  const registerable = await FetchApiData(`${connection.isRegisterable + searchVal}.tez`);
  return registerable.isRegisterable;
}

export async function getHashName(searchVal) {
  const hashName = await FetchApiData(connection.hashName + searchVal);
  return `0x${hashName.hword}`;
}

export async function checkIsExpired(searchVal) {
  const expired = await FetchApiData(`${connection.isExpired + searchVal}.tez`);
  return expired.isExpired;
}

export async function getTezNameAddr(searchVal) {
  const tezNameData = await FetchApiData(`${connection.tezNameAddr + searchVal}.tez`);
  return tezNameData.teznameAddr;
}

export async function getTimestamps(teznameAddr) {
  const timestampsData = await FetchApiData(connection.timestamps + teznameAddr);
  return timestampsData;
}

export async function getOwner(teznameAddr) {
  const ownerData = await FetchApiData(connection.ownerUrl + teznameAddr);
  return ownerData.owner;
}

export async function getDest(teznameAddr) {
  const destData = await FetchApiData(connection.destUrl + teznameAddr);
  return destData.dest;
}
