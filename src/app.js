import React from 'react';
import MainLayout from './layouts/mainLayout';
import Routes from './routes';

function App() {
  return (
    <MainLayout>
      <Routes />
    </MainLayout>
  );
}

export default App;
