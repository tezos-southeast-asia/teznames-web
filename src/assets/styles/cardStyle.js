const CardStyle = {
  card: {
    width: '95%',
    marginLeft: '2.5%',
    marginTop: '20px',
    marginBottom: '20px'
  },
  topCard: {
    width: '95%',
    marginLeft: '2.5%',
    marginTop: '100px',
    marginBottom: '20px'
  },
  cardContent: {
    paddingTop: '0 !important'
  }
};

export default CardStyle;
