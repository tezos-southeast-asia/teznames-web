import { createMuiTheme } from '@material-ui/core/styles';

const Theme = createMuiTheme({
  palette: {
    background: { dark: '#212121' },
    text: { light: '#ffffff' },
    secondary: { main: '#64c7f4' }
  }
});

export default Theme;
