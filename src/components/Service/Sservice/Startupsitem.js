import React from 'react';
import Reveal from 'react-reveal';

function Startupsitem(props) {
  const { Sicon, text, description, iconClass } = props || '';
  return (
    <Reveal bottom cascade>
      <div className="startup_service_item">
        <div className={`icon ${iconClass}`}>
          <i className={`${Sicon}`} />
        </div>
        <h3>{text}</h3>
        <p>{description}</p>
      </div>
    </Reveal>
  );
}
export default Startupsitem;
