import React from 'react';

function StserviceItem(props) {
  const { iShap, Sicon, text, description, btn, bicon, nClass } = props || '';
  return (
    <div className={`p_service_item agency_service_item pr_70 wow fadeInUp ${nClass}`}>
      <div className="icon">
        <img src={require(`../../../assets/img/home4/${iShap}`)} alt="" />
        <i className={Sicon} />
      </div>
      <h5 className="f_600 f_p t_color3">{text}</h5>
      <p>{description}</p>
      <p className="mb-0">
        <a href=".#">{btn}</a>
        <i className={bicon} />
      </p>
    </div>
  );
}
export default StserviceItem;
