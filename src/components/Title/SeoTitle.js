import React from 'react';
import Reveal from 'react-reveal/Reveal';

function SeoTitle(props) {
  const { Title, TitleP } = props || '';
  return (
    <div className="seo_sec_title text-center mb_70">
      <Reveal effect="fadeInUp" duration={1300}>
        <h2>{Title}</h2>
      </Reveal>
      <Reveal effect="fadeInUp" duration={1600}>
        <p>{TitleP}</p>
      </Reveal>
    </div>
  );
}
export default SeoTitle;
