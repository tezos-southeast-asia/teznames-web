import React from 'react';
import PropTypes from 'prop-types';
import Highlight from 'react-highlight.js';

function CodeHighLighter(props) {
  const { hidden, code } = props;
  return (
    <div hidden={hidden}>
      <Highlight style={{ width: '100%' }} language="bash">
        {code}
      </Highlight>
    </div>
  );
}

CodeHighLighter.propTypes = {
  hidden: PropTypes.bool.isRequired,
  code: PropTypes.string.isRequired
};

export default CodeHighLighter;
