import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { CopyToClipboard } from 'react-copy-to-clipboard';

export default function CopyCodeBtn(props) {
  const { value, handleClick } = props;
  return (
    <CopyToClipboard text={value}>
      <Button variant="contained" onClick={handleClick} size="small" color="secondary">
        Copy to clipboard
      </Button>
    </CopyToClipboard>
  );
}

CopyCodeBtn.propTypes = {
  handleClick: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired
};
