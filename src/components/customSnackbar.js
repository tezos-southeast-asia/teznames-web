import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarWrapper from './snackbarWrapper';
import { hideSnackBar } from '../redux/root/actions';

function CustomSnackbar() {
  const dispatch = useDispatch();
  const rootState = useSelector(state => state.Root);
  const { snackbar } = rootState;
  const { open, msg, variant } = snackbar;

  const handleClose = (_event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    dispatch(hideSnackBar());
  };

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left'
      }}
      open={open}
      autoHideDuration={2000}
      onClose={handleClose}
    >
      {snackbar ? <SnackbarWrapper onClose={handleClose} variant={variant} message={msg} /> : null}
    </Snackbar>
  );
}

export default CustomSnackbar;
