import React from 'react';
import { useSelector } from 'react-redux';
import LinearProgress from '@material-ui/core/LinearProgress';

function ProgressBar() {
  const hideLoading = useSelector(state => state.Root.hideLoading);
  return <LinearProgress color="secondary" hidden={hideLoading} />;
}

export default ProgressBar;
