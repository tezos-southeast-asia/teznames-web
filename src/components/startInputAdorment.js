import React from 'react';
import PropTypes from 'prop-types';
import InputAdornment from '@material-ui/core/InputAdornment';

export default function StartInputAdorment(props) {
  const { icon } = props;
  return <InputAdornment position="start">{icon}</InputAdornment>;
}

StartInputAdorment.propTypes = {
  icon: PropTypes.element.isRequired
};
