import { makeStyles } from '@material-ui/core/styles';
import { amber, green } from '@material-ui/core/colors';

const useDrawerStyles = makeStyles(theme => ({
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: theme.palette.error.dark,
    color: theme.palette.text.light
  },
  info: {
    backgroundColor: theme.palette.background.dark,
    color: theme.palette.text.light
  },
  copy: {
    backgroundColor: theme.palette.background.dark,
    color: theme.palette.text.light
  },
  warning: {
    backgroundColor: amber[700],
    color: theme.palette.text.light
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1)
  },
  message: {
    display: 'flex',
    alignItems: 'center'
  },
  margin: {
    margin: theme.spacing(1)
  }
}));

export default useDrawerStyles;
