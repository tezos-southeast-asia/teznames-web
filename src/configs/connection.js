const source = 'https://api.tezos.id/mooncake/babylonnet/teznames/v0.4/babylon/';

const connection = {
  isRegistered: `${source}status/isRegistered?name=`,
  isExpired: `${source}status/isExpired?name=`,
  isRegisterable: `${source}status/isRegisterable?name=`,
  tezNameAddr: `${source}info/getTeznameAddr?name=`,
  ownerUrl: `${source}info/getOwner?address=`,
  destUrl: `${source}info/getDest?address=`,
  registrationList: `${source}info/registrationList`,
  timestamps: `${source}info/timestamps?address=`,
  gateAddr: `${source}getGateAddr`,
  hashName: `${source}hashWord?word=`
};

export default connection;
