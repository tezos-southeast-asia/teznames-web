import axios from 'axios';

const fetchApi = async (apiOptions = {}) => {
  const { url, ...restOptions } = apiOptions;
  const defaultOptions = {
    timeout: 30000,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
    data: {},
    params: {}
  };

  if (!url) {
    return {
      error: 'No API URL'
    };
  }

  const mergedOptions = {
    url,
    ...defaultOptions,
    ...restOptions
  };
  const response = await axios.request(mergedOptions);
  return response.data;
};

const FetchApiData = url => fetchApi({ url });

export default FetchApiData;
