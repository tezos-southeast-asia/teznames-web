export function checkSearchValForTez(searchVal) {
  const searchArr = searchVal.split('.');
  let searchValue;
  if (searchArr.length > 1) {
    if (searchArr[searchArr.length - 1] === 'tez') {
      searchArr.pop();
      searchValue = searchArr.join('.');
    } else {
      searchValue = searchArr.join('.');
    }
  } else {
    searchValue = searchArr.join('.');
  }
  return searchValue;
}
