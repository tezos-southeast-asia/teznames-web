import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';

import './assets/themify-icon/themify-icons.css';
import './assets/simple-line-icon/simple-line-icons.css';
import './assets/elagent/style.css';
import './assets/animate.css';
import './assets/main.css';
import './assets/responsive.css';
import 'bootstrap/dist/js/bootstrap.min';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

ReactDOM.render(<App />, document.getElementById('root'));
