import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import CustomSnackbar from '../components/customSnackbar';
import store from '../redux/stores';
import theme from '../assets/themes/theme';
import FooterTwo from '../components/Footer/FooterTwo';
import FooterData from '../components/Footer/FooterData';
// import Header from './mainLayout/header';
import Footer from './mainLayout/footer';
import CustomNavbar from './mainLayout/CustomNavbar';

function MainLayout(props) {
  const { children } = props;

  return (
    <ThemeProvider theme={theme}>
      <div className="body_wrapper">
        <Router basename={process.env.PUBLIC_URL}>
          <Provider store={store}>
            <CssBaseline />
            <CustomNavbar cClass="custom_container p0" hbtnClass="new_btn" />
            {/* <Header /> */}
            {children}
            <CustomSnackbar />
            <FooterTwo FooterData={FooterData} />
            <Footer />
          </Provider>
        </Router>
      </div>
    </ThemeProvider>
  );
}

MainLayout.propTypes = {
  children: PropTypes.node.isRequired
};

export default MainLayout;
