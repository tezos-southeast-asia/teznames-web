import React from 'react';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Divider from '@material-ui/core/Divider';
import useStyles from './style';

export default function Footer() {
  const classes = useStyles();

  return (
    <footer className={classes.footer}>
      <Divider style={{ margin: '24px auto', width: 60 }} />
      <Typography variant="body2" color="textSecondary" align="center">
        Copyright ©
        <Link color="inherit" href="https://www.teznames.com/">
          tezNames
        </Link>{' '}
        {new Date().getFullYear()}.
      </Typography>
    </footer>
  );
}
