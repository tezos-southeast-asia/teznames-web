import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link as RouterLink } from 'react-router-dom';
import useStyles from './style';
import ProgressBar from '../../components/progressBar';

function Header() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar>
          <Typography variant="h6" className={classes.title} component={RouterLink} to="/">
            tezNames
          </Typography>
        </Toolbar>
        <ProgressBar />
      </AppBar>
    </div>
  );
}

export default Header;
