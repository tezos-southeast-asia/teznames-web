import { makeStyles } from '@material-ui/core/styles';

import { blue } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginBottom: '80px'
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1,
    color: '#ffffff',
    textDecoration: 'none'
  },
  list: {
    width: 250
  },
  fullList: {
    width: 'auto'
  },
  lin1k: {
    marginLeft: '10px',
    fontWeight: '400',
    color: blue.A400
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(3)
  }
}));

export default useStyles;
