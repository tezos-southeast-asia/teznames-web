import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';

import { Link as RouterLink } from 'react-router-dom';
import logo from '../../assets/images/teznames_logo.jpg';
// import { useSelector, useDispatch } from "react-redux";

const useStyles = makeStyles({
  center: {
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  textAlignment: {
    textAlign: 'center'
  },
  innerBox: {
    marginLeft: '5%',
    width: '90%',
    marginBottom: '15px'
  },
  button: {
    margin: '0 0 3% 5%'
  },
  code: {
    width: '95%'
  }
});

function Cart() {
  // const cart = useSelector(state => state.cart);
  // const dispatch = useDispatch();
  //   function addToCart() {
  //     let found = false;
  //     for (let item of cart) {
  //       if (item.name === result) {
  //         found = true;
  //         break;
  //       }
  //     }
  //     if (!found && available) {
  //       alert("Item Successfully Added")
  //       dispatch({ type: "ADD_ITEM", data: { name: result, price: 10 } });
  //     }else{
  //       alert("Item Already Added to Cart!")
  //     }
  //   }
  const classes = useStyles();
  const loading = false;
  return (
    <Paper>
      <img src={logo} alt="logo" className={classes.center} />

      <Grid item xs={12} hidden={!loading}>
        <Box className={classes.innerBox} border={1}>
          <h3 className={classes.textAlignment}>Loading...</h3>
        </Box>
      </Grid>
      <br />
      <Button
        variant="contained"
        size="large"
        color="secondary"
        className={classes.button}
        component={RouterLink}
        to="/"
      >
        Back
      </Button>
    </Paper>
  );
}

export default Cart;
