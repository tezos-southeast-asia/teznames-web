import React from 'react';
import Breadcrumb from '../../components/Breadcrumb';
import Contacts from '../../components/Contacts';

function About() {
  return (
    <div className="body_wrapper">
      <Breadcrumb
        breadcrumbClass="breadcrumb_area"
        imgName="breadcrumb/banner_bg.png"
        Ptitle="Contact Us"
        Pdescription='Here are a few ways to get in contact with us if you still have questions after reviewing our Readme file" (link to the documentation url)'
      />
      <Contacts />
    </div>
  );
}
export default About;
