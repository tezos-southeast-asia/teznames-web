import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Link as RouterLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { useStyles } from './style';

function FourZeroFour() {
  const classes = useStyles();
  return (
    <>
      <br />
      <Card className={classes.card}>
        <CardContent>
          <Typography gutterBottom variant="h6" component="h6">
            Page not found :(
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Maybe the page you are looking for has been removed, or you typed in the wrong URL
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" color="primary" component={RouterLink} to="/">
            Go To Homepage
          </Button>
        </CardActions>
      </Card>
    </>
  );
}

export default FourZeroFour;
