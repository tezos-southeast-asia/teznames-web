import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  card: {
    maxWidth: '90%',
    marginLeft: '5%'
  }
});

export { useStyles };
