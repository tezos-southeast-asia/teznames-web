import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

// styles
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import SearchRounded from '@material-ui/icons/SearchRounded';
import DescriptionIcon from '@material-ui/icons/Description';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import { useStyles } from './style';
import { checkIsRegistered, getWaitListObj } from '../../api/teznames';
import { checkSearchValForTez } from '../../helpers/search';
import { setSearchVal, submitSearch } from '../../redux/search/actions';
import { setHideLoading, displaySnackBar } from '../../redux/root/actions';

import logo from '../../assets/images/teznames_logo.jpg';
import home1 from '../../assets/images/home1.png';
import home2 from '../../assets/images/home2.png';
import StartInputAdorment from '../../components/startInputAdorment';

function Home() {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const searchState = useSelector(state => state.Search);
  const { searchVal, searchEnabled, matchSearchReg } = searchState;

  const handleSearchInputChange = e => {
    dispatch(setSearchVal({ searchVal: e.target.value }));
  };

  const validateRegex = async () => {
    if (matchSearchReg === false) {
      dispatch(displaySnackBar(true, 'Invalid tezName', 'error'));
    } else {
      dispatch(setHideLoading(false));
      try {
        const newSearchVal = checkSearchValForTez(searchVal.toLowerCase());
        const isRegistered = await checkIsRegistered(newSearchVal);
        const waitListObj = await getWaitListObj(newSearchVal);
        dispatch(submitSearch(newSearchVal, isRegistered, waitListObj));
        if (isRegistered || waitListObj.onWaitList) {
          history.push('/tezname-info');
        } else {
          history.push('/registration');
        }
      } catch (error) {
        dispatch(displaySnackBar(true, 'An error has occur, please try again later', 'error'));
        dispatch(setHideLoading(true));
      }
    }
  };

  const handleSearchClick = () => {
    if (searchEnabled) {
      validateRegex();
    }
  };

  const handleSearchKeyUp = e => {
    if (e.which === 13 && searchEnabled) {
      validateRegex();
    }
  };

  return (
    <Paper style={{ minHeight: '100%', marginTop: '100px' }}>
      <img src={logo} alt="logo" className={classes.center} />
      <TextField
        id="ownerText"
        label="Enter your name"
        placeholder="name.tez"
        type="search"
        className={classes.textField}
        onKeyUp={handleSearchKeyUp}
        onChange={handleSearchInputChange}
        margin="normal"
        variant="outlined"
        InputProps={{
          startAdornment: <StartInputAdorment icon={<SearchRounded />} />
        }}
      />
      <Button
        variant="contained"
        size="large"
        color="primary"
        className={classes.button}
        onClick={handleSearchClick}
        startIcon={<SearchRounded />}
      >
        Search
      </Button>
      <Grid container>
        <Grid item sm={4} xs={12}>
          <img src={home1} alt="" className={classes.image} />
        </Grid>
        <Grid item sm={8} xs={12}>
          <div className={classes.content}>
            <Typography variant="h5">What is tezNames</Typography>
            <Divider className={classes.divider} />
            <Typography variant="subtitle1" color="textSecondary">
              tezNames is a next-generation name service on the tezos blockchain that incorporates
              enhanced functionality to tackle the practical needs of a constantly evolving set of
              challenges in the Internet namespace. We hope to build a stable name registration
              platform which allows innovation to unlock new possibilities in name management,
              valuation, interoperability and not to mention security.
            </Typography>
            <br />
            <Button
              variant="contained"
              href="https://teznames.readthedocs.io/en/latest/"
              color="secondary"
              target="_blank"
              startIcon={<DescriptionIcon />}
            >
              Read the docs
            </Button>
          </div>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item sm={8} xs={12}>
          <div className={classes.content}>
            <Typography variant="h5">Replace Tezos address with a human readable name</Typography>
            <Divider className={classes.divider} />
            <Typography variant="subtitle1" color="textSecondary">
              To send tezzies, all you need to know is the recipient’s blockchain domain
            </Typography>
            <Typography variant="subtitle1" color="textSecondary">
              <b>No more worrying about sending to the wrong address</b>
            </Typography>
          </div>
        </Grid>
        <Grid item sm={4} xs={12}>
          <img src={home2} alt="" className={classes.image} />
        </Grid>
      </Grid>
      <Grid container className={classes.bottom}>
        <Grid item xs={12}>
          <Typography variant="h5" style={{ textAlign: 'center' }}>
            Get your tezName today
          </Typography>
        </Grid>
      </Grid>
    </Paper>
  );
}

export default Home;
