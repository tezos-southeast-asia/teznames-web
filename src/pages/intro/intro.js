import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import SearchRounded from '@material-ui/icons/SearchRounded';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

// import Breadcrumb from '../../components/Breadcrumb';
import Featuresitems from '../../components/Featuresitems';
import AgencyBanner from '../../components/Banner/AgencyBanner';
import BannerData from '../../components/Banner/BannerData';
import { useStyles } from './style';
import { checkIsRegistered, getWaitListObj } from '../../api/teznames';
import { checkSearchValForTez } from '../../helpers/search';
import { setSearchVal, submitSearch } from '../../redux/search/actions';
import { setHideLoading, displaySnackBar } from '../../redux/root/actions';
import StartInputAdorment from '../../components/startInputAdorment';

function Intro() {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const searchState = useSelector(state => state.Search);
  const { searchVal, searchEnabled, matchSearchReg } = searchState;

  const handleSearchInputChange = e => {
    dispatch(setSearchVal({ searchVal: e.target.value }));
  };

  const validateRegex = async () => {
    if (matchSearchReg === false) {
      dispatch(displaySnackBar(true, 'Invalid tezName', 'error'));
    } else {
      dispatch(setHideLoading(false));
      try {
        const newSearchVal = checkSearchValForTez(searchVal.toLowerCase());
        const isRegistered = await checkIsRegistered(newSearchVal);
        const waitListObj = await getWaitListObj(newSearchVal);
        dispatch(submitSearch(newSearchVal, isRegistered, waitListObj));
        if (isRegistered || waitListObj.onWaitList) {
          history.push('/tezname-info');
        } else {
          history.push('/registration');
        }
      } catch (error) {
        dispatch(displaySnackBar(true, 'An error has occur, please try again later', 'error'));
        dispatch(setHideLoading(true));
      }
    }
  };

  const handleSearchClick = () => {
    if (searchEnabled) {
      validateRegex();
    }
  };

  const handleSearchKeyUp = e => {
    if (e.which === 13 && searchEnabled) {
      validateRegex();
    }
  };
  return (
    <div className="body_wrapper">
      <AgencyBanner BannerData={BannerData} />
      <TextField
        id="ownerText"
        label="Enter your name"
        placeholder="name.tez"
        type="search"
        className={classes.textField}
        onKeyUp={handleSearchKeyUp}
        onChange={handleSearchInputChange}
        margin="normal"
        variant="outlined"
        InputProps={{
          startAdornment: <StartInputAdorment icon={<SearchRounded />} />
        }}
      />
      <Button
        variant="contained"
        size="large"
        color="primary"
        className={classes.button}
        onClick={handleSearchClick}
        startIcon={<SearchRounded />}
      >
        Search
      </Button>
      <section className="process_area bg_color sec_pad">
        <div className="container">
          <div className="features_info">
            <img className="dot_img" src={require('../../assets/img/home4/divider.png')} alt="" />
            <Featuresitems
              rowClass="row flex-row-reverse"
              aClass="pr_70 pl_70"
              fimage="process_1.png"
              iImg="icon01.png"
              ftitle="Look up the availability of your .tez name"
              descriptions='.tez names that are available for registration will be shown as "Available" by our smart contract <<insert the smart contract address here>>. If your intended name is not available, fret not! We will show you when the name is next available to be registered should the existing owner not want to renew it'
            />
            <Featuresitems
              rowClass="row"
              aClass="pl_100"
              fimage="process_2.png"
              iImg="icon02.png"
              ftitle="Get information about an existing name"
              descriptions="Through our lookup utility, you can find out whois (pun intended) the owner of an existing .tez name, registration details and which address it is resolving to"
            />
            <Featuresitems
              rowClass="row flex-row-reverse"
              aClass="pr_70 pl_70"
              fimage="process_3.png"
              iImg="icon3.png"
              ftitle="Registering a .tez name is easy"
              descriptions="Simply follow our instructions here (link to getting started page) to interact with our smart contract from your tz1 account. If available, your .tez name will be ready in minutes!"
            />
            <Featuresitems
              rowClass="row"
              aClass="pl_100"
              fimage="process_4.png"
              iImg="icon_04.png"
              ftitle="Manage .tez name"
              descriptions="You can now perform a few commands on your .tez name, such as pointing the name to a tz1/KT1 address in our smart contract resolver, transfer a name or renew it when the time comes"
            />
            <Featuresitems
              rowClass="row flex-row-reverse"
              aClass="pr_70 pl_70"
              fimage="process_5.png"
              iImg="icon_05.png"
              ftitle="Share the word!"
              descriptions="Tell your friends about your spanking new .tez name and get them to also try it out! Even better, tell your wallet provider that they can now lookup yourname.tez or wallet.yourname.tez to resolve into a tz1 address, saving everyone valuable time and from costly mistakes
              We always want to hear from you at help@teznames.com so please let us know what you think"
            />
            <div className="dot middle_dot">
              <span className="dot1" />
              <span className="dot2" />
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Intro;
