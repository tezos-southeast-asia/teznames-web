import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  center: {
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  textField: {
    marginLeft: '20% !important',
    width: '60%'
  },
  button: {
    margin: '20px 0 1% 20% !important'
  },
  divider: {
    margin: '20px 0 20px 0',
    width: 200
  },
  errSnackBar: {
    backgroundColor: `${theme.palette.error.dark} !important`
  },
  errMsg: {
    display: 'flex',
    alignItems: 'center'
  },
  errIcon: {
    opacity: 0.9,
    marginRight: theme.spacing(1)
  },
  image: {
    width: '99%'
  },
  content: {
    margin: '5%',
    padding: '5%'
  },
  bottom: {
    backgroundColor: theme.palette.background.dark,
    color: theme.palette.text.light,
    padding: '5px 0 10px 0'
  }
}));

export { useStyles };
