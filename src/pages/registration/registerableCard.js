import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { useStyles } from './style';

function RegisterableCard(props) {
  const classes = useStyles();
  const { registerable, searchValue } = props;
  const availableTxt = `${searchValue}.tez is available.`;
  const notAvailableTxt = `${searchValue}.tez is not registerable.`;

  return registerable ? (
    <Card className={classes.topCard}>
      <CardHeader title={availableTxt} subheader="Cost: 10 xtz" />
      <CardContent className={classes.cardContent}>
        Register now by simply following the instruction below. (On Babylonnet only)
      </CardContent>
      <CardActions>
        {/* <Button variant="contained" size="small" color="primary">
          Add to Cart
        </Button> */}
        <Button variant="contained" size="small" color="secondary" component={RouterLink} to="/">
          Back
        </Button>
      </CardActions>
    </Card>
  ) : (
    <Card raised className={classes.topCard}>
      <CardHeader title={notAvailableTxt} subheader="Cost: 10 xtz" />
      <CardContent className={classes.cardContent}>Please try another tezName.</CardContent>
      <CardActions>
        <Button variant="contained" size="small" color="secondary" component={RouterLink} to="/">
          Back
        </Button>
      </CardActions>
    </Card>
  );
}

RegisterableCard.propTypes = {
  registerable: PropTypes.bool.isRequired,
  searchValue: PropTypes.string.isRequired
};

export default RegisterableCard;
