import React, { useState, useEffect, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Highlight from 'react-highlight.js';

import TextField from '@material-ui/core/TextField';
import ComputerIcon from '@material-ui/icons/Computer';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import { checkRegisterable, getHashName } from '../../api/teznames';
import { setRegistrationInfo, clearAll } from '../../redux/search/actions';
import { setHideLoading, displaySnackBar } from '../../redux/root/actions';
import { useStyles } from './style';

import StartInputAdorment from '../../components/startInputAdorment';
import RegisterableCard from './registerableCard';
import CopyCodeBtn from '../../components/copyCodeBtn';

function Registration() {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const searchState = useSelector(state => state.Search);
  const { searchVal, tezNameInfo } = searchState;
  const { isRegisterable, hashName } = tezNameInfo;

  const [tezosNode, setTezosNode] = useState('<tezos node>');
  const [walletAddr, setWalletAddr] = useState('<your wallet account>');

  const fetchData = useCallback(async () => {
    try {
      const isRegisterableData = await checkRegisterable(searchVal);
      const hashNameData = await getHashName(searchVal);
      dispatch(setRegistrationInfo(isRegisterableData, hashNameData));
    } catch (error) {
      dispatch(displaySnackBar(true, 'An error has occur, please try again later', 'error'));
    } finally {
      dispatch(setHideLoading(true));
    }
  }, [dispatch, searchVal]);

  const handleTezosNode = e => {
    setTezosNode(e.target.value);
  };

  const handleWalletAddrChange = e => {
    setWalletAddr(e.target.value);
  };

  const handleCopyCodeBtnClick = () => {
    dispatch(dispatch(displaySnackBar(true, 'Copied to clipboard', 'copy')));
  };

  useEffect(() => {
    if (searchVal === '') {
      history.push('/');
    }
    fetchData();

    return () => {
      dispatch(clearAll());
    };
  }, [fetchData, dispatch, searchVal, history]);

  const purchaseCode =
    '$ ' +
    `tezos-client -A ${tezosNode} \\\ntransfer 10 from ${walletAddr} \\\nto KT1AVZKhHkaf1h8ewT2JbpXC49u8xP1K6erz \\\n--entrypoint 'entry_register' \\\n--arg '(Pair "${searchVal}.tez" (Pair ${hashName} (Left Unit)))'`;

  return (
    <>
      <RegisterableCard registerable={isRegisterable} searchValue={searchVal} />
      <Card className={classes.card}>
        <CardHeader title="Manual registration" subheader="CLI Command" />
        <CardContent className={classes.cardContent}>
          <h3>Input this command into your Tezos Client</h3>
          <Highlight language="bash">{purchaseCode}</Highlight>
        </CardContent>
        <CardActions>
          <CopyCodeBtn value={purchaseCode} handleClick={handleCopyCodeBtnClick} />
        </CardActions>
      </Card>
      <Card className={classes.card}>
        <CardHeader title="Registration Form" subheader="For Manual registration" />
        <CardContent className={classes.cardContent}>
          <TextField
            id="tezosNode"
            className={classes.inputs}
            label="Tezos node"
            placeholder="Enter tezos node url"
            type="text"
            margin="normal"
            onChange={handleTezosNode}
            InputProps={{
              startAdornment: <StartInputAdorment icon={<ComputerIcon />} />
            }}
          />
          <TextField
            id="walletAddrTxt"
            className={classes.inputs}
            label="Wallet Address"
            placeholder="tz1..."
            type="text"
            margin="normal"
            onChange={handleWalletAddrChange}
            InputProps={{
              startAdornment: <StartInputAdorment icon={<AccountBalanceWalletIcon />} />
            }}
          />
        </CardContent>
      </Card>
    </>
  );
}

export default Registration;
