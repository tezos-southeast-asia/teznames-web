import { makeStyles } from '@material-ui/core/styles';
import CardStyle from '../../assets/styles/cardStyle';

const useStyles = makeStyles({
  ...CardStyle,
  inputs: {
    width: '100%'
  }
});

export { useStyles };
