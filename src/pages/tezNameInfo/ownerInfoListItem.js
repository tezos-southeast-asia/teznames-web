import React from 'react';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import CachedIcon from '@material-ui/icons/Cached';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExploreIcon from '@material-ui/icons/Explore';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import TodayIcon from '@material-ui/icons/Today';
import EventBusyIcon from '@material-ui/icons/EventBusy';

export default function OwnerInfoListItem(props) {
  const { className, icon, primaryTxt, secondaryEle } = props;
  return (
    <>
      <ListItem>
        <ListItemAvatar>
          <Avatar className={className}>
            <ListItemIcon icon={icon} />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          style={{ overflowX: 'auto' }}
          primary={primaryTxt}
          secondary={secondaryEle}
        />
      </ListItem>
      <Divider variant="inset" component="li" />
    </>
  );
}

function ListItemIcon(props) {
  switch (props.icon) {
    case 'owner':
      return <AccountCircleIcon />;
    case 'dest':
      return <ExploreIcon />;
    case 'registrationDate':
      return <EventAvailableIcon />;
    case 'modificationDate':
      return <TodayIcon />;
    case 'expirationDate':
      return <EventBusyIcon />;
    case 'registrationInProcess':
      return <CachedIcon />;
    default:
      return <AccountCircleIcon />;
  }
}

OwnerInfoListItem.defaultProps = {
  secondaryEle: undefined
};

OwnerInfoListItem.propTypes = {
  className: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  primaryTxt: PropTypes.string.isRequired,
  secondaryEle: PropTypes.oneOfType([PropTypes.func, PropTypes.string, PropTypes.object])
};

ListItemIcon.propTypes = {
  icon: PropTypes.string.isRequired
};
