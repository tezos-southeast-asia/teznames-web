import { makeStyles } from '@material-ui/core/styles';
import { deepPurple, indigo, pink, lightBlue, amber, red } from '@material-ui/core/colors';
import CardStyle from '../../assets/styles/cardStyle';

const useStyles = makeStyles({
  ...CardStyle,
  indigoAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: `${indigo[600]}!important`
  },
  deepPurpleAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: `${deepPurple[600]}!important`
  },
  lightBlueAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: `${lightBlue[600]}!important`
  },
  amberAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: `${amber[600]}!important`
  },
  pinkAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: `${pink[600]}!important`
  },
  redAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: `${red[500]}!important`
  },
  list: {
    width: '100%'
  },
  radioGroup: {
    marginBottom: '30px'
  },
  input: {
    width: '100%'
  }
});

export { useStyles };
