import React, { useState, useEffect, useCallback } from 'react';
import Moment from 'react-moment';
import 'moment-timezone';
import { useDispatch, useSelector } from 'react-redux';
import { Link as RouterLink, useHistory } from 'react-router-dom';

import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import ComputerIcon from '@material-ui/icons/Computer';
import ExploreIcon from '@material-ui/icons/Explore';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import { setHideLoading, displaySnackBar } from '../../redux/root/actions';
import { setRegisteredInfo, clearAll } from '../../redux/search/actions';
import {
  checkIsExpired,
  getTezNameAddr,
  getTimestamps,
  getOwner,
  getDest
} from '../../api/teznames';
import { useStyles } from './style';

import StartInputAdorment from '../../components/startInputAdorment';
import OwnerInfoListItem from './ownerInfoListItem';
import CodeHighlighter from '../../components/codeHighLighter';
import CopyCodeBtn from '../../components/copyCodeBtn';

function TezNameInfo() {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const searchState = useSelector(state => state.Search);
  const { searchVal, tezNameInfo } = searchState;
  const {
    owner,
    dest,
    teznameAddr,
    onWaitList,
    isExpired,
    registrationDate,
    expirationDate,
    modificationDate
  } = tezNameInfo;

  const [radioVal, setRadioVal] = useState('renewal');
  const [tezosNode, setTezosNode] = useState(' <tezos node>');
  const [newDest, setNewDest] = useState('<new destination>');
  const [newWalletAddr, setNewWalletAddr] = useState('<new owner wallet address>');
  const [title, setTitle] = useState('Loading...');

  const updateDest =
    '$ ' +
    `tezos-client -A ${tezosNode} \\\ntransfer 0 from ${owner} \\\nto ${teznameAddr} \\\n--entrypoint 'entry_update' \\\n--arg 'Right "${newDest}"'`;
  const tfsOwner =
    '$ ' +
    `tezos-client -A ${tezosNode} \\\ntransfer 0 from ${owner} \\\nto ${teznameAddr} \\\n--entrypoint 'entry_update' \\\n--arg 'Left "${newWalletAddr}"'`;

  const renewtezName =
    '$ ' +
    `tezos-client -A ${tezosNode} \\\ntransfer 10 from ${owner}\\\nto ${teznameAddr} \\\n--entrypoint 'entry_renew' \\\n--arg 'Unit'`;

  const fetchData = useCallback(async () => {
    try {
      const isExpiredData = await checkIsExpired(searchVal);
      if (onWaitList) {
        dispatch(
          setRegisteredInfo({
            isExpired: isExpiredData,
            tezNameAddr: '<Not available>'
          })
        );
      } else {
        const tezNameAddrData = await getTezNameAddr(searchVal);
        const timestamps = await getTimestamps(tezNameAddrData);
        const ownerData = await getOwner(tezNameAddrData);
        const destData = await getDest(tezNameAddrData);
        dispatch(
          setRegisteredInfo({
            isExpired: isExpiredData,
            tezNameAddr: tezNameAddrData,
            owner: ownerData,
            dest: destData,
            registrationDate: timestamps.registrationDate,
            expirationDate: timestamps.expirationDate,
            modificationDate: timestamps.modificationDate
          })
        );
      }
    } catch (error) {
      dispatch(displaySnackBar(true, 'An error has occur, please try again later', 'error'));
    } finally {
      dispatch(setHideLoading(true));
    }
  }, [dispatch, onWaitList, searchVal]);

  useEffect(() => {
    if (searchVal === '') {
      history.push('/');
    }
    setTitle(`${searchVal}.tez is not available`);
    fetchData();
    return () => {
      dispatch(clearAll());
    };
  }, [setTitle, fetchData, dispatch, searchVal, history]);

  const handleRadioChange = (_event, newValue) => {
    setRadioVal(newValue);
  };

  const handleTezosNode = e => {
    setTezosNode(e.target.value);
  };

  const handleNewDestChange = e => {
    setNewDest(e.target.value);
  };

  const handleNewWalletAddrChange = e => {
    setNewWalletAddr(e.target.value);
  };

  const copyCode = () => {
    switch (radioVal) {
      case 'renewal':
        return renewtezName;
      case 'updateDest':
        return updateDest;
      case 'tfsOwner':
        return tfsOwner;
      default:
        return renewtezName;
    }
  };

  const handleCopyCodeBtnClick = () => {
    dispatch(dispatch(displaySnackBar(true, 'Copied to clipboard', 'copy')));
  };

  return (
    <>
      <Card className={classes.topCard}>
        <CardHeader title={title} subheader="tezName Info (Babylonnet)" />
        <CardContent className={classes.cardContent}>
          <List className={classes.list}>
            <OwnerInfoListItem
              className={classes.indigoAvatar}
              icon="owner"
              primaryTxt="Owner"
              secondaryEle={owner}
            />
            <OwnerInfoListItem
              className={classes.deepPurpleAvatar}
              icon="dest"
              primaryTxt="Destination"
              secondaryEle={dest}
            />
            <div hidden={onWaitList}>
              <OwnerInfoListItem
                className={classes.lightBlueAvatar}
                icon="registrationDate"
                primaryTxt="Registrated Date"
                secondaryEle={<Moment format="LLL">{registrationDate}</Moment>}
              />
              <OwnerInfoListItem
                className={classes.amberAvatar}
                icon="modificationDate"
                primaryTxt="Last Modified Date"
                secondaryEle={<Moment fromNow>{modificationDate}</Moment>}
              />
              <OwnerInfoListItem
                className={classes.pinkAvatar}
                icon="expirationDate"
                primaryTxt="Expiration Date"
                secondaryEle={
                  <>
                    <Moment format="LLL">{expirationDate}</Moment>
                    <br />
                    <Moment diff={new Date()} unit="days">
                      {expirationDate}
                    </Moment>{' '}
                    days left
                    <span hidden={!isExpired}>(Expired)</span>
                  </>
                }
              />
            </div>
            <div hidden={!onWaitList}>
              <OwnerInfoListItem
                className={classes.pinkAvatar}
                icon="registrationInProcess"
                primaryTxt="Registration in process.."
                secondaryEle="This tezNames is being registred"
              />
            </div>
          </List>
        </CardContent>
        <CardActions>
          <Button variant="contained" size="small" color="secondary" component={RouterLink} to="/">
            Back
          </Button>
        </CardActions>
      </Card>

      <Card className={classes.card}>
        <CardHeader title="tezName Management" subheader="CLI Command" />
        <CardContent className={classes.cardContent}>
          <div className={classes.radioGroup}>
            <RadioGroup
              aria-label="commands"
              name="commands"
              value={radioVal}
              onChange={handleRadioChange}
              style={{ display: 'inline' }}
            >
              <FormControlLabel value="renewal" control={<Radio />} label="Renewal" />
              <FormControlLabel
                value="updateDest"
                control={<Radio />}
                label="Update Destination"
              />
              <FormControlLabel value="tfsOwner" control={<Radio />} label="Transfer Ownership" />
            </RadioGroup>
          </div>
          <CodeHighlighter hidden={!(radioVal === 'renewal')} code={renewtezName} />
          <CodeHighlighter hidden={!(radioVal === 'updateDest')} code={updateDest} />
          <CodeHighlighter hidden={!(radioVal === 'tfsOwner')} code={tfsOwner} />
        </CardContent>
        <CardActions>
          <CopyCodeBtn value={copyCode()} handleClick={handleCopyCodeBtnClick} />
        </CardActions>
      </Card>

      <Card className={classes.card}>
        <CardHeader title="Registration Form" subheader="For Manual registration" />
        <CardContent className={classes.cardContent}>
          <TextField
            id="walletAddrTxt"
            label="Tezos node"
            placeholder="Enter tezos node url"
            onChange={handleTezosNode}
            type="text"
            className={classes.input}
            margin="normal"
            InputProps={{
              startAdornment: <StartInputAdorment icon={<ComputerIcon />} />
            }}
          />
          <div hidden={!(radioVal === 'updateDest')}>
            <TextField
              id="newDest"
              label="New Destination"
              placeholder="new destination"
              onChange={handleNewDestChange}
              type="text"
              className={classes.input}
              margin="normal"
              InputProps={{
                startAdornment: <StartInputAdorment icon={<ExploreIcon />} />
              }}
            />
          </div>
          <div hidden={!(radioVal === 'tfsOwner')}>
            <TextField
              id="newWalletAcc"
              label="New Wallet Account"
              placeholder="tz1..."
              onChange={handleNewWalletAddrChange}
              type="text"
              className={classes.input}
              margin="normal"
              InputProps={{
                startAdornment: <StartInputAdorment icon={<AccountBalanceWalletIcon />} />
              }}
            />
          </div>
        </CardContent>
      </Card>
    </>
  );
}

export default TezNameInfo;
