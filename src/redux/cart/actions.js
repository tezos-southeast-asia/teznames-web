export const types = {
  ADD_ITEM: 'ADD_ITEM',
  REMOVE_ITEM: 'REMOVE_ITEM',
  EMPTY_CART: 'EMPTY_CART'
};

export function addItem(data) {
  return { type: types.ADD_ITEM, data };
}

export function removeItem(data) {
  return { type: types.REMOVE_ITEM, data };
}

export function emptyCart() {
  return { type: types.EMPTY_CART };
}
