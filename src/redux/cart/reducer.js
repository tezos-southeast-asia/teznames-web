import { types } from './actions';

const initialState = {
  cart: []
};

export default function cartReducer(state = initialState, action) {
  switch (action.type) {
    case types.ADD_ITEM:
      return {
        ...state,
        cart: state.cart.push(action.data)
      };
    case types.REMOVE_ITEM:
      return {
        ...state,
        cart: state.cart.filter(obj => obj.name !== action.data.name)
      };
    case types.EMPTY_CART:
      return {
        ...state,
        cart: []
      };
    default:
      return state;
  }
}
