import Root from './root/reducer';
import Search from './search/reducer';
import Cart from './cart/reducer';

export default {
  Root,
  Search,
  Cart
};
