export const types = {
  HIDE_LOADING: 'HIDE_LOADING',
  DISPLAY_SNACK_BAR: 'DISPLAY_SNACK_BAR',
  HIDE_SNACK_BAR: 'HIDE_SNACK_BAR'
};

export function setHideLoading(data) {
  return { type: types.HIDE_LOADING, data };
}

export function displaySnackBar(open, msg, variant) {
  const data = { open, msg, variant };
  return { type: types.DISPLAY_SNACK_BAR, data };
}

export function hideSnackBar() {
  return { type: types.HIDE_SNACK_BAR };
}
