import { types } from './actions';

const initialState = {
  hideLoading: true,
  snackbar: {
    open: false,
    msg: '',
    variant: 'info'
  }
};

export default function rootReducer(state = initialState, action) {
  switch (action.type) {
    case types.HIDE_LOADING:
      return {
        ...state,
        hideLoading: action.data
      };
    case types.DISPLAY_SNACK_BAR:
      return {
        ...state,
        snackbar: {
          ...action.data
        }
      };
    case types.HIDE_SNACK_BAR:
      return {
        ...state,
        snackbar: {
          ...state.snackbar,
          open: false
        }
      };
    default:
      return state;
  }
}
