export const types = {
  SET_SEARCH_VAL: 'SET_SEARCH_VAL',
  SET_REGISTRATION_INFO: 'SET_REGISTRATION_INFO',
  SET_REGISTERED_INFO: 'SET_REGISTERED_INFO',
  SUBMIT_SEARCH: 'SUBMIT_SEARCH',
  CLEAR_ALL: 'CLEAR_ALL'
};

export function setSearchVal(data) {
  return { type: types.SET_SEARCH_VAL, data };
}

export function setRegistrationInfo(isRegisterable, hashName) {
  const data = { isRegisterable, hashName };
  return { type: types.SET_REGISTRATION_INFO, data };
}

export function submitSearch(searchVal, isRegistered, waitListObj) {
  const data = { searchVal, isRegistered, waitListObj };
  return { type: types.SUBMIT_SEARCH, data };
}

export function setRegisteredInfo(data) {
  return { type: types.SET_REGISTERED_INFO, data };
}

export function clearAll() {
  return { type: types.CLEAR_ALL };
}
