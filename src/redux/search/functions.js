function searchRegax() {
  return new RegExp('^[a-zA-Z0-9]*?(.tez)*$');
}

export function checkEmptySearchVal(searchVal) {
  return !!searchVal.trim();
}

export function checkSearchValRegax(searchVal) {
  return searchRegax().test(searchVal.trim());
}
