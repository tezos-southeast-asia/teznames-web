import { types } from './actions';
import { checkEmptySearchVal, checkSearchValRegax } from './functions';

const initialState = {
  searchVal: '',
  matchSearchReg: false,
  searchEnabled: false,
  tezNameInfo: {
    isRegisterable: false,
    isRegistered: false,
    onWaitList: false,
    isExpired: false,
    teznameAddr: '<teznames address>',
    registrationDate: '2019-10-01T00:00:01Z',
    expirationDate: '2019-10-01T00:00:01Z',
    modificationDate: '2019-10-01T00:00:01Z',
    hashName: '',
    owner: '',
    dest: ''
  }
};

export default function searchReducer(state = initialState, action) {
  switch (action.type) {
    case types.SET_SEARCH_VAL:
      return {
        ...state,
        searchVal: action.data.searchVal,
        searchEnabled: checkEmptySearchVal(action.data.searchVal),
        matchSearchReg: checkSearchValRegax(action.data.searchVal)
      };
    case types.SET_REGISTRATION_INFO:
      return {
        ...state,
        tezNameInfo: {
          ...state.tezNameInfo,
          isRegisterable: action.data.isRegisterable,
          hashName: action.data.hashName
        }
      };
    case types.SUBMIT_SEARCH:
      return {
        ...state,
        searchVal: action.data.searchVal,
        tezNameInfo: {
          ...state.tezNameInfo,
          isRegistered: action.data.isRegistered,
          onWaitList: action.data.waitListObj.onWaitList,
          owner: action.data.waitListObj.owner,
          dest: action.data.waitListObj.dest
        }
      };
    case types.SET_REGISTERED_INFO: {
      return {
        ...state,
        tezNameInfo: {
          ...state.tezNameInfo,
          ...action.data
        }
      };
    }
    case types.CLEAR_ALL:
      return {
        ...initialState
      };
    default:
      return state;
  }
}
