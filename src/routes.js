import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './pages/home/home';
import Registration from './pages/registration/registration';
import TezNameInfo from './pages/tezNameInfo/tezNameInfo';
import FourZeroFour from './pages/error/404';
import Intro from './pages/intro/intro';
import Service from './pages/service/Service';
import Marketing from './pages/digitalMarketing/Digital-marketing';
import Contact from './pages/contact/Contact';

function Routes() {
  return (
    <Switch>
      <Route exact path="/">
        <Intro />
      </Route>
      <Route exact path="/tezname-info">
        <TezNameInfo />
      </Route>
      <Route exact path="/registration">
        <Registration />
      </Route>
      <Route exact path="/search">
        <Home />
      </Route>
      <Route exact path="/service">
        <Service />
      </Route>
      <Route exact path="/marketing">
        <Marketing />
      </Route>
      <Route exact path="/contact">
        <Contact />
      </Route>
      <Route>
        <FourZeroFour />
      </Route>
    </Switch>
  );
}

export default Routes;
